#!/usr/bin/env bats

load ./load.bash

@test "should always return the index.html" {
  run http --print=b --follow GET "$baseUrl"
  assert_success
  assert_output --partial "<base href=\"/\"/>"
}

@test "should always return the index.html with tailing slash" {
  run http --print=b --follow GET "$baseUrl/"
  assert_success
  assert_output --partial "<base href=\"/\"/>"
}

@test "should always return the index.html with random suffix" {
  run http --print=b --follow GET "$baseUrl/not-exits"
  assert_success
  assert_output --partial "<base href=\"/\"/>"
}
