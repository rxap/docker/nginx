#!/usr/bin/env bats

load ./load.bash

@test "should disable cache for index.html file" {
  run http --print=hb --follow GET "$baseUrl/index.html"
  assert_success
  assert_output --partial "<base href=\"/\"/>"
  assert_output --partial "Cache-Control: no-cache"
  assert_output --partial "Expires: Thu, 01 Jan 1970 00:00:01 GMT"
}

@test "should disable cache for any path" {
  run http --print=hb --follow GET "$baseUrl/some/sub/path"
  assert_success
  assert_output --partial "<base href=\"/\"/>"
  assert_output --partial "Cache-Control: no-cache"
  assert_output --partial "Expires: Thu, 01 Jan 1970 00:00:01 GMT"
}

@test "should disable cache for text.txt file" {
  run http --print=hb --follow GET "$baseUrl/text.txt"
  assert_success
  assert_output --partial "some text"
  assert_output --partial "Cache-Control: no-cache"
  assert_output --partial "Expires: Thu, 01 Jan 1970 00:00:01 GMT"
}

