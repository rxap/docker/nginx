#!/bin/bash

BASE_DIR=$(git rev-parse --show-toplevel)

cd "$BASE_DIR" || exit 1

docker build --no-cache -q -t i18n-nginx:base-test .

docker build --no-cache -q -t i18n-nginx:test ./test

docker run -d -p 8585:80 i18n-nginx:test
