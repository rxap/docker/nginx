#!/usr/bin/env bats

load ./load.bash


@test "should serve the text.txt file and not the index.html" {
  run http --print=b --follow GET "$baseUrl/text.txt"
  assert_success
  assert_output --partial "some text"
}

