load ../test_helper/bats-support/load.bash
load ../test_helper/bats-assert/load.bash

ROOT_DOMAIN=${ROOT_DOMAIN:-localhost}

if [ "${ROOT_DOMAIN_PORT:-8585}" -eq "80" ]; then
  baseUrl="http://${ROOT_DOMAIN}"
else
  baseUrl="http://${ROOT_DOMAIN}:${ROOT_DOMAIN_PORT:-8585}"
fi
containerId=""

setup_file() {
  if [ -z "$CI"  ]; then
    containerId=$(bash ./start.sh | tail -n 1)
  else
    echo "Skipping setup in CI"
  fi
}

teardown_file() {
  if [ -z "$CI"  ]; then
    bash ./stop.sh "$containerId"
  else
    echo "Skipping teardown in CI"
  fi
}
