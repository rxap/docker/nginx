ARG NGINX_TAG=alpine

FROM nginx:${NGINX_TAG}

RUN rm -fr /usr/share/nginx/html/*

# Create a dummy index.html file
RUN touch /usr/share/nginx/html/index.html

RUN mkdir -p /utility

RUN ln -s /docker-entrypoint.sh /utility/start.sh

RUN apk --no-cache add curl

COPY mime.types /etc/nginx/additional-mime.types
COPY nginx.conf /etc/nginx/nginx.conf
COPY default.conf.template /etc/nginx/templates/default.conf.template

COPY scheme_logic.conf /etc/nginx/scheme_logic.conf

HEALTHCHECK --interval=30s --timeout=5s --start-period=5s --retries=3 \
    CMD curl --fail --location http://localhost:80/index.html || exit 1
